Meteor.startup(function(){
Mapbox.load({
    // gl: true // optional
    // plugins: list // optional
})
});

Template.map.onRendered(function () {
	$('.mdl-layout__header').addClass('is-casting-shadow')
        this.autorun(function () {
		        if (Mapbox.loaded() ) {
		        		//setup vars
	 					Session.setDefault('mapCenter', {lat: 36.13496335529411, lng: -115.14650344848633});
	 					Session.setDefault('mapZoom', 11);
	 					L.mapbox.accessToken = 'pk.eyJ1Ijoid2FnbmVycGF1bCIsImEiOiJrbkJNLTU4In0.3MjhLjPTh5V_oPoTReLPbg';

	 					try {
		           		LeafletMap = L.mapbox.map('map', 'wagnerpaul.9c0af9ed',{ zoomControl:false, center: Session.get('mapCenter'), zoom: Session.get('mapZoom')  });

	 					} catch(e) {

	 					} finally {
	 									var geojson = Router.current().data()['locations'].fetch()
		    			LeafletMap.featureLayer.setGeoJSON(geojson);
	 					}

			
          
		        } 
		});

	// this.autorun(function () {
 //        if (Mapbox.loaded()) {
 //    mapboxgl.accessToken = 'pk.eyJ1Ijoid2FnbmVycGF1bCIsImEiOiJrbkJNLTU4In0.3MjhLjPTh5V_oPoTReLPbg';
 //   map = new mapboxgl.Map({
 //      container: 'map',
 //      style: 'mapbox://styles/mapbox/streets-v9'
 //  });
 //        } 
 //  });
});
Template.map.onDestroyed(function () {
	$('.mdl-layout__header').removeClass('is-casting-shadow')
	Session.set('mapCenter', LeafletMap.getCenter());
	Session.set('mapZoom', LeafletMap.getZoom());
});

