Template._header.helpers({
  title: function() {
    return Router.current().route.options.name.toUpperCase(); 
  }

});

Template._header.events({
  'click .mdl-layout__drawer.is-visible .mdl-navigation__link': function(e) {
      var layout = document.querySelector('.mdl-layout');
      layout.MaterialLayout.toggleDrawer();
  },
  'click #profile-button': function(e) {
      Router.go('/profile');
  },
});

