Template.locations.onCreated(function () {
    Session.setDefault('contextualNav', 'locations');
    Session.setDefault('locationsView', 'locations_list');
});
Template.locations.onRendered(function () {
    Session.set('contextualNav', 'locations');
});

Template.locations.onDestroyed(function () {
    Session.set('contextualNav', undefined);
});
Template.locations.events({
  'click .mdl-tabs__tab': function(e) {
    if(LeafletMap) {
    	LeafletMap.invalidateSize();
    }
  }
});


Template.locations.helpers({
  isView: function(name) {
    return Session.equals('locationsView', name);
  },
});

