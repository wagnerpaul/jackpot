Template.locations_list.helpers({
  moreResults: function() {
    var ctrl = Iron.controller();
    return ctrl.state.get('limit') < ctrl.count();
  }
});
Template.locations_list.events({
  'click #showMoreResults': function(e) {
    e.preventDefault();
    var ctrl = Iron.controller();
    ctrl.state.set('limit', ctrl.state.get('limit') + ctrl.increment);
  }
});
Template.location.onRendered(function (argument) {
	// var table = document.querySelector('table');
	// var headerCheckbox = table.querySelector('thead .mdl-data-table__select input');
	// var boxes = table.querySelectorAll('tbody .mdl-data-table__select');
	// var headerCheckHandler = function(event) {
	//   if (event.target.checked) {
	//     for (var i = 0, length = boxes.length; i < length; i++) {
	//       boxes[i].MaterialCheckbox.check();
	//     }
	//   } else {
	//     for (var i = 0, length = boxes.length; i < length; i++) {
	//       boxes[i].MaterialCheckbox.uncheck();
	//     }
	//   }
	// };
	// headerCheckbox.addEventListener('change', headerCheckHandler);
})

Template.location.onDestroyed(function (argument) {
	$('.mdl-layout__header').removeClass('is-casting-shadow')
})