

Template.contextualNav.events({
  'click #show-list': function() {
    return Session.set('locationsView', 'locations_list');
  },
   'click #show-map': function() {
    return Session.set('locationsView', 'map');
  }
});

Template.contextualNav.helpers({
  isNav: function(name) {
    return Session.equals('contextualNav', name);
  }
});