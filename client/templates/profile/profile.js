// Template.profile.onRendered(function() {
//     this.autorun(function () {
//         if (AutoForm.getValidationContext('accountForm').isValid()) {
//           // enable submit button
//            console.log('enable')
//         } else {
//           // disable submit button
//           console.log('disable')
//         }
//       });
// });

AutoForm.addHooks('accountForm', {
    onSubmit: function (insertDoc, updateDoc, currentDoc) {
        this.event.preventDefault();
        console.log('before update call');
        return false;
    }
});

Template.testForm.helpers({
  something: Template.instance(),
  schema: function () {
    return Schema.UserProfile
  },
  action: function () {

    return function (els, callbacks, changed) {
     tpl = Template.instance();
        //only submit if things change
        if (Object.keys(changed).length > 0) {
             Meteor.call('updateSelfProfile', this);
              //order matters here if you want succes messages
              callbacks.reset(true);
              callbacks.success();
                   tpl.$('.form-wrapper').addClass("flash");
                setTimeout(function() {
                     tpl.$('.form-wrapper').removeClass("flash");
                }, 1500);
           
        }

    
    };
  },

});

ReactiveForms.createFormBlock({
  template: 'myFormBlock',
  submitType: 'enterKey',
  rendered: function() {
    MaterialTextfield.prototype.checkValidity = function () {
        //do nothing here... we are overloading the method on purpose
    };

  },

});



ReactiveForms.createElement({
  template: 'basicInput',
  validationEvent: 'keyup',
  passThroughData: true,
  
});
