Template.registerHelper('truncate', function(string, length) {
  var cleanString = s(string).stripTags();
  return s(cleanString).truncate(length);
});
Template.registerHelper('properCase', function(string, length) {
  return string.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
});
Template.registerHelper('avatar', function (avatarSize, user) {
     if ( user.emails[0].address ) {
        var email = user.emails[0].address;
     }
    email = email || "none@none.com"; 
    return Gravatar.imageUrl( email, { secure: true, size: avatarSize, d: 'mm' } );  
});