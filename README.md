# jackpot-boilerplate

A starting point for MeteorJS applications. Includes iron-router, Bootstrap 3, Font Awesome, LESS and more.  Forked from https://github.com/Differential/meteor-boilerplate.git

* [Included Packages](#markdown-header-included-packages)
* [Installation](#markdown-header-installation)
* [File Structure](#markdown-header-file-structure)
* [Material Design Lite for Meteor](#markdown-header-material-design-lite-for-meteor)
* [SEO](#markdown-header-seo)
* [Favicons and Touch Icons](#markdown-header-favicons-and-touch-icons)
* [Seed Data](#markdown-header-seed-data)

## [Included Packages](#markdown-header-included-packages)

* Collections:
  * [dburles:collection-helpers](https://github.com/dburles/meteor-collection-helpers)
  * [matb33:collection-hooks](https://github.com/matb33/meteor-collection-hooks)
  * [reywood:publish-composite](https://github.com/englue/meteor-publish-composite)
* Router:
  * [iron:router](https://github.com/EventedMind/iron-router)
  * [zimme:iron-router-active](https://github.com/zimme/meteor-iron-router-active)
  * [yasinuslu:blaze-meta](https://github.com/yasinuslu/blaze-meta)
* Authentication
  * [useraccounts:mdl](https://github.com/meteor-useraccounts/mdl)
  * [alanning:roles](https://github.com/alanning/meteor-roles)
* Seed Data
  * [dburles:factory](https://github.com/percolatestudio/meteor-factory)
  * [anti:fake](https://github.com/anticoders/meteor-fake/)
* [Less](http://lesscss.org)
  * [zodiase:mdl](https://github.com/Zodiase/meteor-mdl)
* Misc:
  * [Moment.js](http://momentjs.com/)
  * [Underscore.js](http://underscorejs.org/)
  * [Underscore.string](http://epeli.github.io/underscore.string/)
  * [cunneen:mailgun](https://github.com/cunneen/meteor-mailgun)
  * [pauloborges:mapbox](https://github.com/pauloborges/meteor-mapbox/)
  * [jparker:gravatar](https://github.com/p-j/meteor-gravatar/)



## [Installation](#markdown-header-installation)

1. Install Meteor - https://www.meteor.com/install

2. Clone this repo to `<yourapp>`

  `git clone https://<your_username>@bitbucket.org/wagnerpaul/jackpot.git <yourapp>`

3. Start Meteor

  `cd <yourapp> && meteor`

4. Start coding!

## [File Structure](#markdown-header-file-structure)

We have a common file structure we use across all of our Meteor apps. Client-only files are stored in the `client` directory, server-only files are stored in the `server` directory, and shared files are stored in the `both` directory. The `private` and `public` directories are for either private or public assets. 

## [Material Design Lite for Meteor](#markdown-header-material-design-lite-for-meteor)

> Material Design Lite lets you add a Material Design look and feel to your websites. It doesn’t rely on any JavaScript frameworks and aims to optimize for cross-device use, gracefully degrade in older browsers, and offer an experience that is immediately accessible.
> - [getmdl.io](http://www.getmdl.io/index.html)

[Visit MDL's website](http://www.getmdl.io/started/index.html) to learn how to get started with MDL.




**(Important change since version `1.0.6_4`)** After installing, create an empty settings file `zodiase-mdl.json` under the root of your app. **Without it most functions will be disabled.**


#### If you want to pick your own theme, like [what the MDL team have here](http://www.getmdl.io/customize/index.html), here's how:

1. First pick your theme colors from that page. You would have to pick both the **primary color** and the **accent color**.

2. Note that on the lower part of that page, there's a link that basically tells you what colors you have picked.

    * The link is always composed by `material.{primary}-{accent}.min.css`. I picked `deep_orange` and `blue` for example, and that link looks like `material.deep_orange-blue.min.css`.

3. Now create a `zodiase-mdl.json` file under the root of your app if you haven't already. This file stores the settings.

4. In that setting file, compose a JSON document that looks similar to this:

    ```JSON
    {
      "theme": {
        "primary": "deep_orange",
        "accent": "blue"
      }
    }
    ```

5. This will tell the package to load the corresponding theme file.

#### If you want more than pre-built themes:

You can load up the SASS source code and define your own colors!

1. First you'd need to tell the package not to load any theme file.

    ```JSON
    {
      "theme": false
    }
    ```

2. Define your own theme colors and load MDL's SASS code.

    ```SASS
    // mdl-theme.scss
    $color-primary: "0,0,0";
    $color-accent: "255,255,255";
    @import '{zodiase:mdl}/theme';
    ```

3. There you go!

4. There's a full range of variables you can customize. Check out [MDL's variables](https://github.com/google/material-design-lite/blob/master/src/_variables.scss) to learn more.

#### If you want to import SASS files from MDL's source code: (Updated in `1.0.6_4`)

They are all under `{zodiase:mdl-assets}/src` so have fun!





## [SEO](#markdown-header-seo)

Page titles, meta descriptions and Facebook and Twitter meta tags are handled by the [yasinuslu:blaze-meta](https://github.com/yasinuslu/blaze-meta) package. Global settings are configured in `both/router/meta.js`, while individual page settings are set at the controller level.

* Note: the `spiderable` package will need to be installed and configured on your server in order for bots to read your meta tags.

```javascript
PostsShowController = AppController.extend({
  path: '/posts/:_id',
  waitOn: function() {
    return this.subscribe('post', params._id);
  },
  data: function() {
    return {
      post: Post.find({_id: params._id})
    };
  },
  onAfterAction: function() {
    if(this.ready()) {
      Meta.setTitle(this.data().post.title);
    }
  }
});
```

## [Favicons and Touch Icons](#markdown-header-favicons-and-touch-icons)

Upload your image to http://realfavicongenerator.net/ and place the resulting images in `public/images/favicons`

## [Seed Data](#markdown-header-seed-data)

You can use the [dburles:factory](https://github.com/percolatestudio/meteor-factory) and [anti:fake](https://github.com/anticoders/meteor-fake/) packages to generate fake collection data for testing your UI. See `server/seeds.js` for an example:

```javascript
Meteor.startup(function() {

  Factory.define('item', Items, {
    name: function() { return Fake.sentence(); },
    rating: function() { return _.random(1, 5); }
  });

  if (Items.find({}).count() === 0) {

    _(10).times(function(n) {
      Factory.create('item');
    });

  }

});

```