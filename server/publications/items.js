Meteor.publishComposite("items",  function(limit) {
  return {
    find: function() {
      //set default limit
      limit = limit || 30;
      return  Items.find({},{limit: limit});
    }
    // ,
    // children: [
    //   {
    //     find: function(item) {
    //       return [];
    //     }
    //   }
    // ]
  }
});
