Meteor.publishComposite("locations", function(limit) {
  return {
    find: function() {
      //set default limit
      limit = limit || 30;
      return Locations.find({},{limit: limit});
    }
    // ,
    // children: [
    //   {
    //     find: function(item) {
    //       return [];
    //     }
    //   }
    // ]
  }
});
