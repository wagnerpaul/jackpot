//using mailgun through the cunneen:mailgun plugins
Meteor.startup(function() {

  Meteor.Mailgun.config({
    username: 'postmaster@domain.com',
    password: 'password-goes-here'
  });

  Meteor.methods({
    'sendContactEmail': function(name, email, message) {
      this.unblock();

      Meteor.Mailgun.send({
        to: 'recipient@example.com',
        from: name + ' <' + email + '>',
        subject: 'New Contact Form Message',
        text: message,
        html: Handlebars.templates['contactEmail']({siteURL: Meteor.absoluteUrl(), fromName: name, fromEmail: email, message: message})
      });
    }
  });
});

//native meteor mailgun stuff
Meteor.startup(function() {
  Accounts.emailTemplates.from = "Jackpot Admin <paulw@foodchain.com>";
  process.env.MAIL_URL = 'smtp://postmaster@niche9.com:e6a39b8ae9ebd132628ba4da65e03f2a@smtp.mailgun.org:587';
});
