Meteor.startup(function() {

  Factory.define('item', Items, {
    name: function() { return Fake.sentence(); },
    rating: function() { return _.random(1, 5); }
  });

  if (Items.find({}).count() < 100) {
  	Items.remove({});
    _(200).times(function(n) {
      Factory.create('item');
    });

  }

});

