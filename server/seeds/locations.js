Meteor.startup(function() {


if (Locations.find().count() === 0) {
        console.log("Importing private/locations.json to db")

        var data = JSON.parse(Assets.getText("locations.json"));
        console.log(data.features);
        data.features.forEach(function (item, index, array) {
            Locations.insert(item);
        })
    }
});

