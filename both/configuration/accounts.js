//routes
AccountsTemplates.configureRoute('signIn', {layoutTemplate: 'loginOut'});
AccountsTemplates.configureRoute('signUp', {layoutTemplate: 'loginOut'});
AccountsTemplates.configureRoute('resetPwd', {layoutTemplate: 'loginOut'});
AccountsTemplates.configureRoute('ensureSignedIn', {layoutTemplate: 'loginOut'});

//config
AccountsTemplates.configure({
    showForgotPasswordLink: true,
});
// the following shows how to add fields to the register screen
// it even inherits the simple schemas tied to users and profiles... Woot!
// AccountsTemplates.addFields([
//     {
//         _id: 'firstname',
//         type: 'text',
//         displayName: "First Name",
//     },
//     {
//         _id: 'lastname',
//         type: 'text',
//         displayName: "Last Name",
//     }
// ]);