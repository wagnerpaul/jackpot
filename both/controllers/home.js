HomeController = AppController.extend({
  increment: 10,
  publishLimit: 60,
    yieldTemplates: {
        'locations_list': { to: 'list' },
        'map': { to: 'map' },
    },

  onBeforeAction: function () {
    this.state.setDefault('limit', 20);
    this.next();
  },
  limit: function () {
    return this.state.get('limit') || this.increment;
  },
  subscriptions: function () {

    return Meteor.subscribe('locations', this.publishLimit);
  },
  data: function() {
    var self = this;
    var data = []
    data['locations'] = Locations.find({}, {sort: { createdAt: -1 }, limit: this.limit()}) ;
    return data;
  },

  count: function () {
    return Locations.find({}).count(); 
    //todo remove Counts plugin
  },
  action: function () {
    if (this.ready()) {
      this.render();
    } else {
      this.next();
    }
  },

  showMoreVisible: function () {
    var ctrl = Router.current();
    var threshold, target = $('#showMoreResults');
    if (target.length) {
      threshold = $(window).scrollTop() + $(window).height() - target.height();
      if (target.offset().top <= threshold) {
        if (!target.data('visible')) {
          target.data('visible', true);
          ctrl.state.set('limit', ctrl.state.get('limit') + ctrl.increment);
        }
      } else {
        if (target.data('visible')) {
          target.data('visible', false);
        }
      }
    }
  }
});