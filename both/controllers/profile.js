ProfileController = AppController.extend({
  waitOn: function() {
  },
  data: function(){
    return Meteor.user();
  },
  onAfterAction: function () {
    Meta.setTitle('Profile');
  },

});

ProfileController.events({
  'click [data-action=doSomething]': function (event, template) {
    event.preventDefault();
  }
});