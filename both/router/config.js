Router.configure({
  controller: 'AppController',
  loadingTemplate: 'loading'
});

// Router.plugin('loading', {loadingTemplate: 'loading'});
Router.plugin('dataNotFound', {dataNotFoundTemplate: 'notFound'});
Router.plugin('ensureSignedIn', {
	//uncomment line below to only protect certain pages
	//only: ['dashboard', 'map','home','locations']
});