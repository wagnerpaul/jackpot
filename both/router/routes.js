
Router.route('/', {
  name: 'locations',
  controller: 'LocationsController'
});

Router.route('/dashboard', {
  name: 'dashboard',
  controller: 'DashboardController'
});

Router.route('/map', {
  name: 'map',
  controller: 'MapController'
});

Router.route('/profile', {
  name: 'profile',
  controller: 'ProfileController'
});









